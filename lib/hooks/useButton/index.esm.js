import { ref } from 'vue'

function useButtonClick() {
	const nub = ref(0)
	nub.value = nub.value + 20
	console.log(nub)
	return { nub }
}

export { useButtonClick }
