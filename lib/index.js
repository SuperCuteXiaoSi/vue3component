import {
	ref,
	defineComponent,
	openBlock,
	createElementBlock,
	toDisplayString,
} from 'vue'

function useButtonClick() {
	const nub = ref(0)
	nub.value = nub.value + 20
	console.log(nub)
	return { nub }
}

var script = defineComponent({
	name: 'IButton',
	props: {},
	setup() {
		const { nub } = useButtonClick()
		return { nub }
	},
})

const _hoisted_1 = { class: 'mybutton' }

function render(_ctx, _cache, $props, $setup, $data, $options) {
	return (
		openBlock(),
		createElementBlock(
			'button',
			_hoisted_1,
			'这个是一个自定义组件来的' + toDisplayString(_ctx.nub),
			1 /* TEXT */
		)
	)
}

script.render = render
script.__file = 'packages/components/button/src/index.vue'

script.install = (app) => {
	app.component(script.name, script)
}
const InMeAccordionItem = script

const components = [InMeAccordionItem]
const install = function (app) {
	components.forEach((component) => app.component(component.name, component))
}
var index = {
	install,
	...components,
}

export { InMeAccordionItem as IButton, index as default }
