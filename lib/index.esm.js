import {
	ref as n,
	defineComponent as t,
	openBlock as o,
	createElementBlock as e,
	toDisplayString as u,
} from 'vue'
var a = t({
	name: 'IButton',
	props: {},
	setup() {
		const { nub: t } = (function () {
			const t = n(0)
			return (t.value = t.value + 20), console.log(t), { nub: t }
		})()
		return { nub: t }
	},
})
const r = { class: 'mybutton' }
;(a.render = function (n, t, a, s, c, l) {
	return o(), e('button', r, '这个是一个自定义组件来的' + u(n.nub), 1)
}),
	(a.__file = 'packages/components/button/src/index.vue'),
	(a.install = (n) => {
		n.component(a.name, a)
	})
const s = a,
	c = [s]
var l = {
	install: function (n) {
		c.forEach((t) => n.component(t.name, t))
	},
	...c,
}
export { s as IButton, l as default }
