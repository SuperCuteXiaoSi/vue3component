import nodeResolve from '@rollup/plugin-node-resolve' // 告诉 Rollup 如何查找外部模块
// import { terser } from 'rollup-plugin-terser'
import typescript from 'rollup-plugin-typescript2'
import vue from 'rollup-plugin-vue' // 处理vue文件
import { readdirSync } from 'fs' // 写文件
import { resolve } from 'path'
const input = resolve(__dirname, '../packages/hooks') // 入口文件
const output = resolve(__dirname, '../lib/xscomponents/hooks') // 输出文件
const config = readdirSync(input).map((name) => ({
	input: `${input}/${name}/index.ts`,
	output: {
		format: 'esm',
		file: `${output}/${name}/index.esm.js`,
	},
	plugins: [
		nodeResolve(),
		vue(),
		typescript({
			tsconfigOverride: {
				compilerOptions: {
					declaration: false,
				},
				exclude: ['node_modules', 'examples', 'mobile', 'tests'],
			},
			abortOnError: false,
			clean: true,
		}),
	],
	external: ['vue'],
}))

export default config
